This is the privacy policy for the Application Flashlight/Torch Widget by Shoof:
- No personal data whatsoever are being collected by the developer or any 
parties thereof.
- The camera permission is only used to gain access to the camera's LED (Flash)
because the app was meant to be backwards compatible with older Android devices.
